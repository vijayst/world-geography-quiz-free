//
//  Translation.h
//  World Geography Quiz Free
//
//  Created by Vijay Thirugnanam on 24/11/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Translation : NSObject
-(NSString *)getTranslation:(NSString *)englishString;
@end
