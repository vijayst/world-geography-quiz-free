//
//  GameOverViewController.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 16/03/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "GameOverViewController.h"
#import "Points.h"
#import "Translation.h"

@interface GameOverViewController ()
{
    Translation *translation;
}
@property (strong, nonatomic) NSString *shareText;
@property (strong, nonatomic) NSURL *shareUrl;
@end

@implementation GameOverViewController

@synthesize game;
@synthesize gameOverLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initLabels];
     self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]];
}

-(void)viewDidAppear:(BOOL)animated
{
    self.facebookButton.hidden = ![SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook];
    self.twitterButton.hidden = ![SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
        
    game.score += [Points getBonusPoints:game.gameType quizType:self.game.quiz.type];
    int maxPoints = [Points getMaximumPoints:game.gameType quizType:self.game.quiz.type];
    
    NSString *gameOverText = [translation getTranslation:@"Congrats! You scored %d out of %d points."];
    gameOverLabel.text = [NSString stringWithFormat:gameOverText, self.game.score, maxPoints];
    
    // TODO: translate this before tweeting!
    NSString *shareTextFormat = [translation getTranslation:@"I scored %d out of %d points in World Geography Quiz - Free Edition"];
    self.shareText = [NSString stringWithFormat:shareTextFormat, game.score, maxPoints];
    // TODO: Get the below URL dynamically
    NSString *urlText = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"App Url"];
    self.shareUrl = [NSURL URLWithString:urlText];
}

-(void)initLabels
{
    translation = [[Translation alloc] init];
    NSString *titleText = [translation getTranslation:@"Game Over"];
    self.titleLabel.text = titleText;
    NSString *mainText = [translation getTranslation:@"Main Menu"];
    [self.mainButton setTitle:mainText forState:UIControlStateNormal];
    NSString *facebookText = [translation getTranslation:@"Share with Facebook"];
    [self.facebookButton setTitle:facebookText forState:UIControlStateNormal];
    NSString *twitterText = [translation getTranslation:@"Share with Twitter"];
    [self.twitterButton setTitle:twitterText forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)showMenu:(id)sender {
    if(self.presentingViewController!=nil)
        [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)shareFacebook:(id)sender
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [controller setInitialText:self.shareText];
        [controller addURL:self.shareUrl];
        [self presentViewController:controller animated:YES completion:Nil];
    }
}


- (IBAction)shareTwitter:(id)sender
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [controller setInitialText:self.shareText];
        [controller addURL:self.shareUrl];
        [self presentViewController:controller animated:YES completion:Nil];
    }
}
@end
