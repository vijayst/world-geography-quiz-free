//
//  DbHelper.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 05/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DbHelper : NSObject
{
}
@property NSString *dbName;
-(id)init;
-(id)initWithDb:(NSString *)dbName;
-(void)copyDbToAppSupport:(BOOL)overwrite;
-(void)open;
-(void)close;
-(bool)executeCommand:(NSString *)sql;
-(bool)executeQuery:(NSString *)sql statement:(sqlite3_stmt **)statement;
-(float)executeReal:(NSString *)sql;
-(BOOL)doesTableExist:(NSString *)tableName;
@end
