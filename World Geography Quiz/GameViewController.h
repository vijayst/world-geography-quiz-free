//
//  ViewController.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

#import "Game.h"
#import "Quiz.h"
#import "AnimationHelper.h"
#import "TTSHelper.h"

@interface GameViewController : UIViewController<AnimationCompletedDelegate, SpeechCompletedDelegate, UIAlertViewDelegate, ADBannerViewDelegate>
{
    bool animComplete;
    bool startNewLevel;
    int startNewLevelTime;
    NSTimer *timer;
    bool iPhone;
    float headerFontSize;
    
    UIColor *matchColor;
    UIColor *noMatchColor;
}

@property (strong, nonatomic) IBOutlet UIView *subView;
@property (strong, nonatomic) IBOutlet UILabel *choice1Label;
@property (strong, nonatomic) IBOutlet UILabel *choice2Label;
@property (strong, nonatomic) IBOutlet UILabel *choice3Label;
@property (strong, nonatomic) IBOutlet UILabel *choice4Label;
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) IBOutlet UILabel *levelLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *highLabel;
@property (strong, nonatomic) IBOutlet UILabel *maxLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (strong, nonatomic) IBOutlet UILabel *questionTextLabel;
- (IBAction)showMenu:(id)sender;

@property (strong, nonatomic) Game* game;
@property (strong, nonatomic) UIImage *capitalImage;
@property (strong, nonatomic) UIImage *flagImage;
@property (strong, nonatomic) UIImage *currencyImage;
@property (strong, nonatomic) UIImage *continentImage;
@end
