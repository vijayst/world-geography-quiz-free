//
//  TTSHelper.m
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 18/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "TTSHelper.h"

@interface TTSHelper()

@end

@implementation TTSHelper
@synthesize delegate;

-(id)init
{
    self = [super init];
    if(self) {
        NSString *language = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Voice Language"];
        voice = [AVSpeechSynthesisVoice voiceWithLanguage:language];
        speech = [[AVSpeechSynthesizer alloc] init];
        speech.delegate = self;
    }
    return self;
}

-(void)speakText:(NSString *)text
{
    AVSpeechUtterance *welcome = [[AVSpeechUtterance alloc] initWithString:text];
    welcome.rate = 0.20;
    welcome.volume = 0.6;
    welcome.voice = voice;
    [speech speakUtterance:welcome];
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance
{
    [delegate speechCompleted];
}

@end
