//
//  AudioHelper.h
//  GDP Jarvis
//
//  Created by Vijay Thirugnanam on 25/11/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVAudioSession.h>

@interface AudioHelper : NSObject<AVAudioPlayerDelegate>
-(void)playAppLoadSound;
-(void)playGameLoadSound;
-(void)playMatchSound;
-(void)playNoMatchSound;

+(void)createAudioSession;
@property (nonatomic, strong) AVAudioPlayer *player;
@end
