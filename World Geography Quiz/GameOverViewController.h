//
//  GameOverViewController.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 16/03/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

#import "Game.h"


@interface GameOverViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *gameOverLabel;
@property (strong, nonatomic) Game *game;
@property (strong, nonatomic) IBOutlet UIButton *facebookButton;
@property (strong, nonatomic) IBOutlet UIButton *twitterButton;
- (IBAction)showMenu:(id)sender;
-(IBAction)shareFacebook:(id)sender;
- (IBAction)shareTwitter:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *mainButton;
@end
