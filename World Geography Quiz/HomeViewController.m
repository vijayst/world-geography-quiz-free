//
//  HomeViewController.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 16/03/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "HomeViewController.h"
#import "GameViewController.h"
#import "GameOverViewController.h"
#import "Translation.h"
#import "AudioHelper.h"
#import "Game.h"

@interface HomeViewController ()
{
    Translation *translation;
    GameViewController *_gameController;
    
    Game *_fullGame;
    Game *_capitalGame;
    Game *_flagGame;
    Game *_currencyGame;
    Game *_continentGame;
    Game *_currentGame;
}
@property (strong, nonatomic) AudioHelper *audioHelper;
-(void)animate;
@property (strong, nonatomic) UIDynamicAnimator *animator;
@end

@implementation HomeViewController

@synthesize fullGameButton;
@synthesize capitalGameButton;
@synthesize flagGameButton;
@synthesize currencyGameButton;
@synthesize continentGameButton;

@synthesize audioHelper;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initLabels];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]];
     CGRect rect = [[UIScreen mainScreen] bounds];
    
    if(rect.size.height==480) {
        float topAdjust = -4.0;
        float leftAdjust = 20.0F;
        float heightAdjust = -16.0F;
        float widthAdjust = -40.0F;
        
        CGRect fullGameFrame = fullGameButton.frame;
        [fullGameButton setFrame:CGRectMake(fullGameFrame.origin.x+leftAdjust, fullGameFrame.origin.y + topAdjust, fullGameFrame.size.width + widthAdjust, fullGameFrame.size.height + heightAdjust)];
        
        CGRect capitalFrame = capitalGameButton.frame;
        [capitalGameButton setFrame:CGRectMake(capitalFrame.origin.x+leftAdjust, capitalFrame.origin.y + topAdjust + heightAdjust, capitalFrame.size.width + widthAdjust, capitalFrame.size.height + heightAdjust)];
        
        CGRect flagFrame = flagGameButton.frame;
        [flagGameButton setFrame:CGRectMake(flagFrame.origin.x+leftAdjust, flagFrame.origin.y + topAdjust + 2*heightAdjust, flagFrame.size.width + widthAdjust, flagFrame.size.height + heightAdjust)];
        
        CGRect currencyFrame = currencyGameButton.frame;
        [currencyGameButton setFrame:CGRectMake(currencyFrame.origin.x+leftAdjust, currencyFrame.origin.y + topAdjust + 3*heightAdjust, currencyFrame.size.width + widthAdjust, currencyFrame.size.height + heightAdjust)];
        
        CGRect continentFrame = continentGameButton.frame;
        [continentGameButton setFrame:CGRectMake(continentFrame.origin.x+leftAdjust, continentFrame.origin.y + topAdjust + 4*heightAdjust, continentFrame.size.width + widthAdjust, continentFrame.size.height + heightAdjust)];
    }

    
    if(audioHelper==nil)
        audioHelper = [[AudioHelper alloc] init];
    [audioHelper playAppLoadSound];
    
    [self animate];
}

-(void)initLabels
{
    translation = [[Translation alloc] init];
    NSString *fullText = [translation getTranslation:@"Full Game"];
    NSString *capitalsText = [translation getTranslation:@"Capitals"];
    NSString *continentsText = [translation getTranslation:@"Continents"];
    NSString *flagsText = [translation getTranslation:@"Flags"];
    NSString *currenciesText = [translation getTranslation:@"Currencies"];
    
    [self.fullGameButton setTitle:fullText forState:UIControlStateNormal];
    [self.capitalGameButton setTitle:capitalsText forState:UIControlStateNormal];
    [self.continentGameButton setTitle:continentsText forState:UIControlStateNormal];
    [self.currencyGameButton setTitle:currenciesText forState:UIControlStateNormal];
    [self.flagGameButton setTitle:flagsText forState:UIControlStateNormal];
}

-(void)animate
{
    CGRect fullGameFrame = self.fullGameButton.frame;
    CGPoint topRight = CGPointMake(fullGameFrame.origin.x + fullGameFrame.size.width,
                                   fullGameFrame.origin.y);
    CGRect capitalFrame = self.capitalGameButton.frame;
    CGRect flagFrame = self.flagGameButton.frame;
    CGRect currencyFrame = self.currencyGameButton.frame;
    
    CGRect continentFrame = continentGameButton.frame;
    CGPoint bottomRight = CGPointMake(continentFrame.origin.x + continentFrame.size.width,
                                      continentFrame.origin.y + continentFrame.size.height);
    
    fullGameFrame.origin.x = - fullGameFrame.size.width - 200.0;
    self.fullGameButton.frame = fullGameFrame;
    capitalFrame.origin.x = - fullGameFrame.size.width - 400.0;
    self.capitalGameButton.frame = capitalFrame;
    flagFrame.origin.x = - fullGameFrame.size.width - 600.0;
    self.flagGameButton.frame = flagFrame;
    currencyFrame.origin.x = - fullGameFrame.size.width - 800.0;
    self.currencyGameButton.frame = currencyFrame;
    continentFrame.origin.x = - fullGameFrame.size.width - 1000.0;
    self.continentGameButton.frame = continentFrame;
    
    NSArray *controlArray = @[self.fullGameButton, self.capitalGameButton, self.flagGameButton, self.currencyGameButton, self.continentGameButton];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    UIGravityBehavior *gravityBehavior = [[UIGravityBehavior alloc] initWithItems:controlArray];
    UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:controlArray];
    collisionBehavior.collisionMode = UICollisionBehaviorModeBoundaries;
    [collisionBehavior addBoundaryWithIdentifier:@"stop" fromPoint:topRight toPoint:bottomRight];
    gravityBehavior.gravityDirection = CGVectorMake(1.0, 0.0);
    UIDynamicItemBehavior *dynamicBehavior = [[UIDynamicItemBehavior alloc] initWithItems:controlArray];
    dynamicBehavior.elasticity = 0.2;
    [self.animator addBehavior:gravityBehavior];
    [self.animator addBehavior:collisionBehavior];
    [self.animator addBehavior:dynamicBehavior];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)fullGameClick:(id)sender {
    if(_fullGame==nil || _fullGame.over)
        _fullGame = [Game createGame:GameTypeMulti andType:QuizTypeNone];
    [self presentGameController:_fullGame];
}

- (IBAction)capitalGameClick:(id)sender {
    if(_capitalGame==nil || _capitalGame.over)
        _capitalGame = [Game createGame:GameTypeSingle andType:Capitals];
    [self presentGameController:_capitalGame];
}

- (IBAction)flagGameClick:(id)sender {
    if(_flagGame==nil || _flagGame.over)
        _flagGame = [Game createGame:GameTypeSingle andType:Flags];
    [self presentGameController:_flagGame];
}

- (IBAction)currencyGameClick:(id)sender {
    if(_currencyGame==nil || _currencyGame.over)
        _currencyGame = [Game createGame:GameTypeSingle andType:Currencies];
    [self presentGameController:_currencyGame];
}

- (IBAction)continentGameClick:(id)sender {
    if(_continentGame==nil || _continentGame.over)
        _continentGame = [Game createGame:GameTypeSingle andType:Continents];
    [self presentGameController:_continentGame];
}

-(void)presentGameController:(Game *)game
{
    _currentGame = game;
    if(_gameController==nil) {
        _gameController = [self.storyboard instantiateViewControllerWithIdentifier:@"gameVcId"];
    }
    _gameController.game = game;
    [self presentViewController:_gameController
                       animated:NO
                     completion:nil];
}

-(void)showGameOver
{
    [self dismissViewControllerAnimated:false completion:^{
        GameOverViewController *overController = [self.storyboard instantiateViewControllerWithIdentifier:@"overVcId"];
        
        overController.game = _currentGame;
        
        [self presentViewController:overController
                           animated:YES
                         completion:nil];
    }];
    
}

@end
