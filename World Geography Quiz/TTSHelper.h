//
//  TTSHelper.h
//  GDPJarvis
//
//  Created by Vijay Thirugnanam on 18/10/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVSpeechSynthesis.h>

@protocol SpeechCompletedDelegate<NSObject>
-(void)speechCompleted;
@end

@interface TTSHelper : NSObject<AVSpeechSynthesizerDelegate>
{
    AVSpeechSynthesisVoice *voice;
    AVSpeechSynthesizer *speech;
}
-(void)speakText:(NSString *)text;
@property (assign, nonatomic) id<SpeechCompletedDelegate> delegate;
@end
