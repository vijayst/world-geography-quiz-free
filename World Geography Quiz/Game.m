#import "Game.h"
#import "Country.h"
#import "Points.h"
#import "Translation.h"

@interface Game()
{
    enum GameType _gameType;
    enum QuizType _quizType;
    int _answerPosition; // 0 - 3
}
@property (strong, nonatomic) NSMutableArray *gameArray;
@property (strong, nonatomic) NSMutableArray *typeArray;
@property (nonatomic, strong) Translation *translation;

-(NSMutableArray *)getSequentialArray:(int)count;
-(void)setGameType:(enum GameType)gameType andQuizType:(enum QuizType)quizType;
-(NSMutableArray *)getCapitals:(NSString *)answer;
-(NSMutableArray *)getFlags:(NSString *)answer;
-(NSMutableArray *)getCurrencies:(NSString *)answer;
-(NSMutableArray *)getContinents:(NSString *)answer;
@end

@implementation Game

@synthesize typeArray;
@synthesize gameArray;
@synthesize countries;
@synthesize score;
@synthesize  level;


+(Game *)createGame:(enum GameType)gameType andType:(enum QuizType)quizType
{
    Game* newGame = [[Game alloc] init];
    if(newGame) {
        [newGame setGameType:gameType andQuizType:quizType];
        newGame.countries = [Country getCountries];
        int count = newGame->count = (int)[newGame.countries count];
        NSMutableArray *capitalsGame = [newGame getSequentialArray:count];
        NSMutableArray *flagsGame = [newGame getSequentialArray:count];
        NSMutableArray *currencyGame = [newGame getSequentialArray:count];
        NSMutableArray *continentsGame = [newGame getSequentialArray:count];
        newGame.gameArray = [NSMutableArray arrayWithArray:@[capitalsGame,
                                                             flagsGame,
                                                             currencyGame,
                                                             continentsGame]];
    }
    return newGame;
}

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        self.translation = [[Translation alloc] init];
        self.level = 0;
        self.score = 0;
        self.over = NO;
    }
    return self;
}


-(enum GameType)gameType
{
    return _gameType;
}

-(int)maxPoints
{
    return [Points getMaximumPoints:_gameType quizType:_quizType];
}

-(void)setGameType:(enum GameType)gameType andQuizType:(enum QuizType)quizType
{
    _gameType = gameType;
    _quizType = quizType;
    
    switch(gameType)
    {
        case GameTypeSingle:
            switch(quizType)
        {
            case Capitals:
                typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Capitals]]];
                break;
            case Flags:
                typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Flags]]];
                break;
            case Currencies:
                typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Currencies]]];
                break;
            case Continents:
                typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Continents]]];
                break;
            case Maps:
            case QuizTypeNone:
                break;
        }
            break;
        case GameTypeMulti:
            typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Capitals],
                                                         [NSNumber numberWithInt:Flags],
                                                         [NSNumber numberWithInt:Currencies],
                                                         [NSNumber numberWithInt:Continents]]];
            break;
    }
}

-(NSMutableArray *)getSequentialArray:(int)pcount
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:pcount];
    for (int i=0; i<pcount; i++) {
        array[i] = [NSNumber numberWithInt:i];
    }
    return array;
}

-(BOOL)getQuiz
{
    // no more quizzes
   if(self.over)
       return NO;
    
    level++;
    int questionType = _quizType;
    if(_gameType == GameTypeMulti) {
        int questionTypeIndex = arc4random() % [typeArray count];
        questionType = [self.typeArray[questionTypeIndex] intValue];
    }
    
    NSMutableArray *quizItems = [gameArray objectAtIndex:questionType];
    int questionNumberIndex = arc4random() % [quizItems count];
    NSNumber *questionNumber = [quizItems objectAtIndex:questionNumberIndex];
    [quizItems removeObjectAtIndex:questionNumberIndex];
    // if last element in the sequence, that sequence is complete.
    if([quizItems count]==0) {
        if(_gameType==GameTypeMulti) {
            [gameArray removeObjectAtIndex:questionType];
            [typeArray removeObjectAtIndex:questionType];
            if([typeArray count]==0) {
                self.over = YES;
            }
        }
        else {
            self.over = YES;
        }
    }
    
    question = [questionNumber intValue];
    Country *country = [countries objectAtIndex:question];
    
    NSMutableArray *answerArray;
    NSString *prompt;
    
    // question type is different from game type.
    // game type can be full
    // question type is 0 - 3.
    switch(questionType)
    {
        case Capitals:
            answerArray = [self getCapitals:country.capital];
            prompt = [self.translation getTranslation:@"What is the capital of %@?"];
            break;
        case Flags:
            answerArray = [self getFlags:country.flag];
            prompt = [self.translation getTranslation:@"Identify the flag of %@?"];
            break;
        case Currencies:
            answerArray = [self getCurrencies:country.currency];
            prompt = [self.translation getTranslation:@"What is the currency of %@?"];
            break;
        case Continents:
            answerArray = [self getContinents:country.continent];
            prompt = [self.translation getTranslation:@"Which continent does %@ belong to?"];
            break;
        default:
            break;
    }
    
    prompt = [NSString stringWithFormat:prompt, country.name];
    
    
    difficulty = country.difficulty;
    
    self.quiz = [[Quiz alloc] init];
    self.quiz.question = country.name;
    self.quiz.choice1 = [answerArray objectAtIndex:0];
    self.quiz.choice2 = [answerArray objectAtIndex:1];
    self.quiz.choice3 = [answerArray objectAtIndex:2];
    self.quiz.choice4 = [answerArray objectAtIndex:3];
    self.quiz.answer = _answerPosition;
    self.quiz.type = questionType;
    self.quiz.prompt = prompt;
    self.quiz.level = level;
    self.quiz.difficulty = country.difficulty;
    return YES;
}

-(NSMutableArray *)getCapitals:(NSString *)answer
{
    return [self getItems:^(Country *c)
            {
                return c.capital;
            } withAnswer:answer];
}

-(NSMutableArray *)getFlags:(NSString *)answer
{
    return [self getItems:^(Country *c)
            {
                return c.flag;
            } withAnswer:answer];
}

-(NSMutableArray *)getCurrencies:(NSString *)answer
{
    return [self getItems:^(Country *c)
            {
                return c.currency;
            } withAnswer:answer];
}

-(NSMutableArray *)getContinents:(NSString *)answer
{
    return [self getItems:^(Country *c)
            {
                return c.continent;
            } withAnswer:answer];
}

-(NSMutableArray *)getItems:(NSString *(^)(Country *c))lambda withAnswer:(NSString *)answer
{
    NSMutableSet *answerSet = [[NSMutableSet alloc] init];
    while([answerSet count] < 3) {
        Country *country = [countries objectAtIndex:arc4random() % count];
        NSString *choice = lambda(country);
        if(![choice isEqualToString:answer])
            [answerSet addObject:choice];
    }
    NSMutableArray *answerArray = [[answerSet allObjects] mutableCopy];
    _answerPosition = arc4random() % 4;
    [answerArray insertObject:answer atIndex:_answerPosition];
    return answerArray;
}


@end
