//
//  ViewController.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "GameViewController.h"
#import "HomeViewController.h"
#import "AudioHelper.h"
#import "Points.h"
#import "Translation.h"


@interface GameViewController ()
{
    Translation *translation;
    float iPhoneHeaderFont;
    float iPadHeaderFont;
}
@property (strong, nonatomic) NSString *timeText;
@property (strong, nonatomic) NSString *scoreText;
@property (strong, nonatomic) NSString *levelText;
@property (strong, nonatomic) NSString *maxText;
@property (strong, nonatomic) AudioHelper *audioHelper;
@property (strong, nonatomic) TTSHelper *ttsHelper;
@property (strong, nonatomic) AnimationHelper *animHelper;
-(void)didTapLabelWithGesture:(UITapGestureRecognizer *)sender;
@end

@implementation GameViewController
{
    ADBannerView *_bannerView;
    CGRect _contentFrame;
    BOOL _showAd;
    UIColor *_panelColor;
    UIColor *_timeoutColor;
}

static float flagImageMargin = 15.0F;

#pragma mark ViewController methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initLabels];
	
    _contentFrame = self.view.bounds;
    
    _showAd = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"Show Ad"] boolValue];
    
    if(_showAd) {
        _bannerView = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
        _bannerView.delegate = self;
        
        // required for auto-resizing when moving from portrait to landscape
        // not required for this app, but will keep it.
        [_bannerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        CGRect bannerFrame = _bannerView.frame;
        bannerFrame.origin.y = 0 - bannerFrame.size.height;
        _bannerView.frame = bannerFrame;
        [self.view addSubview:_bannerView];
    }
    
    matchColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.5];
    noMatchColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.8];

    
    CGRect rect = [[UIScreen mainScreen] bounds];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]];
    if(rect.size.height==480) {
        // iPhone layout for 5S is in the storyboard.
        // iPhone layout for older versions.
        CGRect questionFrame = self.questionLabel.frame;
        [self.questionLabel setFrame:CGRectMake(questionFrame.origin.x, questionFrame.origin.y, questionFrame.size.width, questionFrame.size.height-25)];
        CGRect imageFrame = self.imageView.frame;
        [self.imageView setFrame:CGRectMake(imageFrame.origin.x, imageFrame.origin.y-6, imageFrame.size.width-12, imageFrame.size.height-12)];
        CGRect choice1Frame = self.choice1Label.frame;
        [self.choice1Label setFrame:CGRectMake(choice1Frame.origin.x, choice1Frame.origin.y-25, choice1Frame.size.width, choice1Frame.size.height-25)];
        CGRect choice2Frame = self.choice2Label.frame;
        [self.choice2Label setFrame:CGRectMake(choice2Frame.origin.x, choice2Frame.origin.y-25, choice2Frame.size.width, choice2Frame.size.height-25)];
        CGRect choice3Frame = self.choice3Label.frame;
        [self.choice3Label setFrame:CGRectMake(choice3Frame.origin.x, choice3Frame.origin.y-50, choice3Frame.size.width, choice3Frame.size.height-25)];
        CGRect choice4Frame = self.choice4Label.frame;
        [self.choice4Label setFrame:CGRectMake(choice4Frame.origin.x, choice4Frame.origin.y-50, choice4Frame.size.width, choice4Frame.size.height-25)];
        
        CGRect levelFrame = self.levelLabel.frame;
        [self.levelLabel setFrame:CGRectMake(levelFrame.origin.x, levelFrame.origin.y-88, levelFrame.size.width, levelFrame.size.height)];
        CGRect timeFrame = self.timeLabel.frame;
        [self.timeLabel setFrame:CGRectMake(timeFrame.origin.x, timeFrame.origin.y-88, timeFrame.size.width, timeFrame.size.height)];
        CGRect homeFrame = self.homeButton.frame;
        [self.homeButton setFrame:CGRectMake(homeFrame.origin.x, homeFrame.origin.y-88, homeFrame.size.width, homeFrame.size.height)];
        CGRect highFrame = self.highLabel.frame;
        [self.highLabel setFrame:CGRectMake(highFrame.origin.x, highFrame.origin.y-88, highFrame.size.width, highFrame.size.height)];
        CGRect maxFrame = self.maxLabel.frame;
        [self.maxLabel setFrame:CGRectMake(maxFrame.origin.x, maxFrame.origin.y-88, maxFrame.size.width, maxFrame.size.height)];
        [self.view setNeedsDisplay];
    }

    self.capitalImage = [self resize:[UIImage imageNamed:@"capitals.png"]];
    self.flagImage = [self resize:[UIImage imageNamed:@"flags.png"]];
    self.currencyImage = [self resize:[UIImage imageNamed:@"currencies.png"]];
    self.continentImage = [self resize:[UIImage imageNamed:@"continents.png"]];
    
    iPhone = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone;
    if(self.audioHelper==nil)
        self.audioHelper = [[AudioHelper alloc] init];
    self.ttsHelper = [[TTSHelper alloc] init];
    self.ttsHelper.delegate = self;
    self.animHelper = [[AnimationHelper alloc] init];
    self.animHelper.delegate = self;
    
    self.choice1Label.textColor = [UIColor whiteColor];
    self.choice2Label.textColor = [UIColor whiteColor];
    self.choice3Label.textColor = [UIColor whiteColor];
    self.choice4Label.textColor = [UIColor whiteColor];
    
    [self.choice1Label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)]];
    [self.choice2Label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)]];
    [self.choice3Label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)]];
    [self.choice4Label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)]];
    
    self.view.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(!self.game.paused)
        [self getDataForNewLevel];
    else {
        // refresh the view based on the game state.
        [self refreshView];
        [self updateLabels];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    if(_showAd && _bannerView.bannerLoaded)
        [self bannerViewDidLoadAd:_bannerView];
    if(self.game.paused)
        [self startTimer];
    else
    {
        self.view.hidden = NO;
        self.view.userInteractionEnabled = NO;
        self.homeButton.enabled = NO;
        [self animate];
    }
}

-(BOOL)getDataForNewLevel
{
    BOOL result = [self.game getQuiz];
    if(!result) {
        id controller = (HomeViewController *)self.presentingViewController;
        [controller showGameOver];
        return NO;
    }
    
    [self refreshView];
    return YES;
}

-(void)refreshView
{
    self.questionTextLabel.text =  self.game.quiz.question;
    [self setImage:self.game.quiz.type];
    self.choice4Label.text = self.choice3Label.text
    = self.choice2Label.text
    = self.choice1Label.text
    = @"";
    
    
    
    UIColor *labelColor;
    switch(self.game.quiz.type)
    {
        case Capitals:
            labelColor = [UIColor colorWithPatternImage:self.capitalImage];
            break;
        case Flags:
            labelColor = [UIColor colorWithPatternImage:self.flagImage];
            break;
        case Currencies:
            labelColor = [UIColor colorWithPatternImage:self.currencyImage];
            break;
        case Continents:
            labelColor = [UIColor colorWithPatternImage:self.continentImage];
            break;
        case Maps:
        case QuizTypeNone:
            break;
    }
    
    self.choice1Label.backgroundColor = labelColor;
    self.choice2Label.backgroundColor = labelColor;
    self.choice3Label.backgroundColor = labelColor;
    self.choice4Label.backgroundColor = labelColor;
    
    labelColor = self.game.quiz.time > 0 ? _panelColor : _timeoutColor;
    
    self.levelLabel.backgroundColor = labelColor;
    self.timeLabel.backgroundColor = labelColor;
    self.highLabel.backgroundColor = labelColor;
    self.maxLabel.backgroundColor = labelColor;
    self.homeButton.backgroundColor = labelColor;
    
    headerFontSize = iPhone ? iPhoneHeaderFont : iPadHeaderFont;
    self.levelLabel.attributedText = [self getLevelLabel];
    self.timeLabel.attributedText = [self getTimerLabel];
    self.highLabel.attributedText = [self getScoreLabel];
    self.maxLabel.attributedText = [self getMaxLabel];
}

-(void)animate
{
    [self.audioHelper playGameLoadSound];
    animComplete = false;
    self.choice1Label.enabled = self.choice2Label.enabled = self.choice3Label.enabled = self.choice4Label.enabled = true;
    [self.animHelper zoom:self.questionLabel];
    [self.animHelper zoom:self.questionTextLabel];
    [self.animHelper zoom:self.imageView];
    [self.animHelper fade:self.imageView];
    [self.animHelper fade:self.choice1Label];
    [self.animHelper fade:self.choice2Label];
    [self.animHelper fade:self.choice3Label];
    [self.animHelper fade:self.choice4Label];
}

-(void)newLevel
{
    BOOL result = [self getDataForNewLevel];
    if(result) {
        [self animate];
    }
}

-(void)updateTime
{
    if(startNewLevel) {
        if(++startNewLevelTime==3) {
            [self stopTimer];
            [self newLevel];
        }
    } else {
        if(self.game.quiz.time>0) {
            self.game.quiz.time--;
            self.timeLabel.attributedText = [self getTimerLabel];
            if(self.game.quiz.time==0)
            {
                self.levelLabel.backgroundColor = _timeoutColor;
                self.timeLabel.backgroundColor = _timeoutColor;
                self.highLabel.backgroundColor = _timeoutColor;
                self.maxLabel.backgroundColor = _timeoutColor;
                self.homeButton.backgroundColor = _timeoutColor;
            }
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        self.game = [[Game alloc] init];
        [self newLevel];
    }
}

#pragma mark AnimationCompletedDelegate protocol
-(void)animationCompleted
{
    if(!animComplete) {
        animComplete = true;
        [self.ttsHelper speakText:self.game.quiz.prompt];
    }
}

#pragma mark SpeechCompletedDelegate protocol
-(void)speechCompleted
{
    [self updateLabels];
    self.view.userInteractionEnabled = true;
    self.homeButton.enabled = true;
    // start timer
    startNewLevel = false;
    startNewLevelTime=0;
    [self performSelectorOnMainThread:@selector(startTimer)
                           withObject:nil
                        waitUntilDone:YES];
}

#pragma mark Timer methods
-(void)startTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self
                                           selector:@selector(updateTime)
                                           userInfo:Nil repeats:true];
}

-(void)stopTimer
{
    [timer invalidate];
    timer = nil;
}

-(void)pauseGame
{
    [self stopTimer];
    self.game.paused = YES;
}

-(void)resumeGame
{
    if(self.game.paused)
    {
        [self startTimer];
        self.game.paused = NO;
    }
}

-(UIImage *)resize:(UIImage *)image
{
    float width = self.choice1Label.bounds.size.width;
    float height = self.choice1Label.bounds.size.height;
    UIGraphicsBeginImageContextWithOptions(self.choice1Label.bounds.size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark Choice Label methods
-(void)updateLabels
{
    self.choice1Label.enabled = self.choice2Label.enabled
    = self.choice3Label.enabled
    = self.choice4Label.enabled
    = YES;
    
    if(self.game.quiz.type!=Flags) {
        self.choice1Label.text = self.game.quiz.choice1;
        self.choice2Label.text = self.game.quiz.choice2;
        self.choice3Label.text = self.game.quiz.choice3;
        self.choice4Label.text = self.game.quiz.choice4;
    } else {
        [self updateLabel:self.choice1Label imageName:self.game.quiz.choice1 drawType:0];
        [self updateLabel:self.choice2Label imageName:self.game.quiz.choice2 drawType:0];
        [self updateLabel:self.choice3Label imageName:self.game.quiz.choice3 drawType:0];
        [self updateLabel:self.choice4Label imageName:self.game.quiz.choice4 drawType:0];
    }
    
    NSArray *labelArray = @[self.choice1Label, self.choice2Label, self.choice3Label, self.choice4Label];
    for(int i=0; i < 4; i++)
    {
        BOOL enabled = [self.game.quiz.enabledArray[i] boolValue];
        if(!enabled) {
            UILabel *label = (UILabel *)labelArray[i];
            label.enabled = NO;
            if(self.game.quiz.type==Flags) {
                [self updateLabel:label drawType:2];
            }
            else {
                label.backgroundColor = noMatchColor;
            }
        }
    }
}

-(void)updateLabel:(UILabel *)label drawType:(int)drawType
{
    switch(label.tag)
    {
        case 0:
            [self updateLabel:label
                    imageName:self.game.quiz.choice1
                     drawType:drawType];
            break;
        case 1:
            [self updateLabel:label
                    imageName:self.game.quiz.choice2
                     drawType:drawType];
            break;
        case 2:
            [self updateLabel:label
                    imageName:self.game.quiz.choice3
                     drawType:drawType];
            break;
        case 3:
            [self updateLabel:label
                    imageName:self.game.quiz.choice4
                     drawType:drawType];
            break;
    }
}

-(void)updateLabel:(UILabel *)label imageName:(NSString *)imageName drawType:(int)drawType
{
    NSString *fullName = [NSString stringWithFormat:@"%@.png", imageName];
    UIImage *image = [UIImage imageNamed:fullName];
    float scale = image.size.width / image.size.height;
    
    CGSize size = label.frame.size;
    float labelScale = (size.width - 2*flagImageMargin) / (size.height - 2*flagImageMargin);
    UIGraphicsBeginImageContext( size );
    
    // draw the original flag background
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0,0,size.width, size.height);
    if(drawType==0)
        [self.flagImage drawInRect:rect];
    else if(drawType==1) {
        CGContextSetFillColorWithColor(context, [matchColor CGColor]);
        CGContextFillRect(context, rect);
    } else if(drawType==2) {
        CGContextSetFillColorWithColor(context, [noMatchColor CGColor]);
        CGContextFillRect(context, rect);
    }
    
    // scale the flag and draw within a narrow boundary
    if(scale > labelScale) {
    float newHeight = (size.width - 2*flagImageMargin) / scale;
    float newOrigin = (size.height-newHeight)/2;
    [image drawInRect:CGRectMake(flagImageMargin,newOrigin,size.width - 2*flagImageMargin,newHeight)];
    } else {
        float newWidth = (size.height - 2*flagImageMargin)* scale;
        float newOrigin = (size.width-newWidth)/2;
        [image drawInRect:CGRectMake(newOrigin,flagImageMargin,newWidth,size.height-2*flagImageMargin)];
    }
    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    label.backgroundColor = [UIColor colorWithPatternImage:newImage];
}

-(void)setImage:(enum QuizType)type
{
    switch(type)
    {
        case Capitals:
            self.imageView.image = [UIImage imageNamed:@"CapitalsButtonIcon.png"];
            break;
        case Flags:
            self.imageView.image = [UIImage imageNamed:@"FlagsButtonIcon.png"];
            break;
        case Currencies:
            self.imageView.image = [UIImage imageNamed:@"CurrenciesButtonIcon.png"];
            break;
        case Continents:
            self.imageView.image = [UIImage imageNamed:@"ContinentsButtonIcon.png"];
            break;
        case Maps:
        case QuizTypeNone:
            break;
    }
}

-(void)didTapLabelWithGesture:(UITapGestureRecognizer *)sender
{
    if(startNewLevel)
        return;
    
    UILabel *label = (UILabel *)sender.view;
    if(sender) {
        if(label.tag==self.game.quiz.answer) {
            self.view.userInteractionEnabled = false;
            self.homeButton.enabled = false;
            self.game.quiz.attempts++;
            if(self.game.quiz.type!=Flags)
                label.backgroundColor =  matchColor;
            else
                [self updateLabel:label drawType:1];
            [self.audioHelper playMatchSound];
            int points = [Points calculate:self.game.quiz.type
                                   attempt:self.game.quiz.attempts
                                      time:self.game.quiz.time
                                difficulty:self.game.quiz.difficulty];
            
            self.game.score += points;
            self.highLabel.attributedText = [self getScoreLabel];
            startNewLevel = true;
            NSLog(@"%d", points);
            
        } else {
            if([self.game.quiz.enabledArray[label.tag] boolValue]) {
                if(self.game.quiz.type!=Flags)
                    label.backgroundColor = noMatchColor;
                else
                    [self updateLabel:label drawType:2];
                
                self.game.quiz.attempts++;
                [self.audioHelper playNoMatchSound];
                self.game.quiz.enabledArray[label.tag] = @NO;
            }
        }
    }
}

#pragma mark Panel Label methods
-(void)initLabels
{
    translation = [[Translation alloc] init];
    self.timeText = [translation getTranslation:@"Time"];
    self.levelText = [translation getTranslation:@"Level"];
    self.maxText = [translation getTranslation:@"Max"];
    self.scoreText = [translation getTranslation:@"Score"];
    
    static NSString *iPhoneFontKey = @"iPhone Game Label Header Font";
    static NSString *iPadFontKey = @"iPad Game Label Header Font";
    iPhoneHeaderFont = [[[NSBundle mainBundle] objectForInfoDictionaryKey:iPhoneFontKey] floatValue];
    iPadHeaderFont = [[[NSBundle mainBundle] objectForInfoDictionaryKey:iPadFontKey] floatValue];
    
    _panelColor = [UIColor colorWithRed:0
                                  green:33/256.0
                                   blue:81/256.0
                                  alpha:1];
    
     _timeoutColor = [UIColor colorWithRed:187/256.0
                                     green:21/256.0
                                      blue:27/256.0
                                     alpha:1];
}

-(NSAttributedString *)getLevelLabel
{
    NSInteger len = [self.levelText length];
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n \n%d", self.levelText, self.game.level]];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:headerFontSize] range:NSMakeRange(0,len)];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:5.0F] range:NSMakeRange(len+1,1)];
    return labelText;
}

-(NSAttributedString *)getTimerLabel
{
    NSInteger len = [self.timeText length];
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n \n%d", self.timeText, self.game.quiz.time]];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:headerFontSize] range:NSMakeRange(0,len)];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:5.0F] range:NSMakeRange(len+1,1)];
    return labelText;
}

-(NSAttributedString *)getScoreLabel
{
    NSInteger len = [self.scoreText length];
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n \n%d", self.scoreText, self.game.score]];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:headerFontSize] range:NSMakeRange(0,len)];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:5.0F] range:NSMakeRange(len+1,1)];
    return labelText;
}

-(NSAttributedString *)getMaxLabel
{
    NSInteger len = [self.maxText length];
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n \n%d", self.maxText, self.game.maxPoints]];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:headerFontSize] range:NSMakeRange(0,len)];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:5.0F] range:NSMakeRange(len+1,1)];
    return labelText;
}

- (IBAction)showMenu:(id)sender {
    [self pauseGame];
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark ADBannerViewDelegate protocol
-(void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    // for UI adjustments when loading AD
    NSLog(@"Received ad");
    CGRect bannerFrame = _bannerView.frame;
    bannerFrame.origin.y = 0;
    CGFloat scale = (_contentFrame.size.height - bannerFrame.size.height) / _contentFrame.size.height;
    CGAffineTransform scaleTransform = CGAffineTransformMakeScale(1,  scale);
    
    // Translation does not work as expected
    // CGAffineTransform moveTransform = CGAffineTransformMakeTranslation(0, bannerFrame.size.height * scale);
    // CGAffineTransform transform = CGAffineTransformConcat(scaleTransform, moveTransform);
    
    [UIView animateWithDuration:0.20 animations:^{
        _bannerView.frame = bannerFrame;
    }];
    
    self.subView.transform = scaleTransform;
    
    self.subView.frame = CGRectMake(_contentFrame.origin.x, _contentFrame.origin.y + bannerFrame.size.height,
                                    _contentFrame.size.width, _contentFrame.size.height - bannerFrame.size.height);
    
    [self.subView setNeedsDisplay];
}

-(BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    [self pauseGame];
    return YES;
}

-(void)bannerViewActionDidFinish:(ADBannerView *)banner
{
    // No action is required.
    // The default action is to resume the game when the view appears.
    [self resumeGame];
}

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Failed to receive ad");
    CGRect bannerFrame = _bannerView.frame;
    bannerFrame.origin.y = 0 - bannerFrame.size.height;
    [UIView animateWithDuration:0.20 animations:^{
        _bannerView.frame = bannerFrame;
    }];
    
    self.subView.transform = CGAffineTransformIdentity;
    self.subView.frame = _contentFrame;
}

@end
