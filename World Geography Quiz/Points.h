//
//  Points.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Quiz.h"
#import "Game.h"

@interface Points : NSObject
+(int)calculate:(enum QuizType)type attempt:(int)attempt time:(int)time difficulty:(int)difficulty;
+(int)getMaximumPoints:(enum GameType)gameType quizType:(enum QuizType)quizType;
+(int)getBonusPoints:(enum GameType)gameType quizType:(enum QuizType)quizType;
@end
